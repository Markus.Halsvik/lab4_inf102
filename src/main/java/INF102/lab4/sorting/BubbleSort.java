package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        for (int n = list.size() - 1; n > 0; n--) {
            for (int i = 0; i < n; i++) {
                T el1 = list.get(i);
                T el2 = list.get(i + 1);
                if (el1.compareTo(el2) > 0) {
                    list.set(i, el2);
                    list.set(i + 1, el1);
                }
            }
        }
        // Bubble sorting moves the largest element, then the second largest and so on
        // to the rightmost side of the list by comparing each element to the next, and swapping if the left is larger.
        // If the list has size n, it first does n - 1 comparisons, then n - 2 and so on, leading up to a total amount
        // of operations equal to n * (n - 1) / 2, or on the order of O(n^2)
    }
}
