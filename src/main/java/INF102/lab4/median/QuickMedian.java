package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        int index = (listCopy.size() - 1) / 2;    // Index of desired element in sorted list

        while (true) { // Not sure how to recursively call function without the index as a parameter, so while loop it is
            int listSize = listCopy.size();
            int randInd = (int) (Math.random() * listSize);  // Index of random element in list
            T pivot = listCopy.get(randInd);                 // Retrieve random element as pivot to split list
            int lessThan = 0;
            int largThan = 0;

            // I first determine the size of the lists before creating them and inserting any elements
            // As these are linear with n, as opposed to simply initializing the arraylists and adding the elements
            for (int i = 0; i < listSize; i++) {
                T compEl = listCopy.get(i);
                if (i != randInd) {
                    if (compEl.compareTo(pivot) < 0) {
                        lessThan++;
                    }
                    else {
                        largThan++;
                    }
                }
            }

            if (lessThan == index) {
                return pivot;
            }

            if (lessThan < index) {    // If the element we want is on the right side of the pivot, cast away the left side including pivot
                index -= lessThan + 1; // Subtract to find index in the new list
                List <T> largList = new ArrayList<T>(largThan);
                largList.add(0, null);
                for (int i = 0; i < listSize; i++) {
                    T compEl = listCopy.get(i);
                    if (compEl.compareTo(pivot) > 0) {
                        if (largList.get(0) != null) {
                            largList.add(compEl);
                        }
                        else {
                            largList.set(0, compEl);
                        }
                    }
                }
                listCopy = largList; // Repeat with new list
            }
            else { // The element we want is on the left-hand side of the pivot, so index remains the same in the new list
                List <T> lessList = new ArrayList<T>(lessThan);
                lessList.add(0, null);
                for (int i = 0; i < listSize; i++) {
                    T compEl = listCopy.get(i);
                    if (compEl.compareTo(pivot) < 0) {
                        if (lessList.get(0) != null) {
                            lessList.add(compEl);
                        }
                        else {
                            lessList.set(0, compEl);
                        }
                    }
                }
                listCopy = lessList; // Repeat with new list
            }
        }
    }
}
